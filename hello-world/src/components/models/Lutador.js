export default class Lutador{
  constructor(JsonApi){
    this.apelido = JsonApi.nickname ? JsonApi.nickname : ""
    this.nome = `${JsonApi.first_name} ${JsonApi.last_name}`
    this.categoria = JsonApi.weight_class
    this.imagem = JsonApi.left_full_body_image
    this.habilidades = [
      {nome: 'Velocidade', valor: Math.floor(Math.random() * (6 - 1) + 1 )},
      {nome: 'Esquiva', valor: Math.floor(Math.random() * (6 - 1) + 1 )},
      {nome: 'Boxe', valor: Math.floor(Math.random() * (6 - 1) + 1 )},
      {nome: 'MuayThai', valor: Math.floor(Math.random() * (6 - 1) + 1 )},
      {nome: 'Wrestling', valor: Math.floor(Math.random() * (6 - 1) + 1 )},
      {nome: 'Resistência', valor: Math.floor(Math.random() * (6 - 1) + 1 )},
      {nome: 'JiuJitsu', valor: Math.floor(Math.random() * (6 - 1) + 1 )}]
    this.experiencia = 0
    this.level = 0
    this.SkillsPoints = 0
  }
}
