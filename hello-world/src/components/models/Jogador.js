
export default class Jogador{
    constructor(nome, apelido, categoria, habilidades){
        this.nome = nome
        this.apelido = apelido
        this.categoria = categoria
        this.imagem = "../../assets/lutador.png"
        this.habilidades = [
            {nome: 'Velocidade', valor: habilidades[0].valor},
            {nome: 'Esquiva', valor: habilidades[1].valor},
            {nome: 'Boxe', valor: habilidades[2].valor},
            {nome: 'MuayThai', valor: habilidades[3].valor},
            {nome: 'Wrestling', valor: habilidades[4].valor},
            {nome: 'Resistência', valor: habilidades[5].valor},
            {nome: 'JiuJitsu', valor: habilidades[6].valor}]
        this.experiencia = 0
        this.level = 0
        this.SkillsPoints = 0
    }

}
