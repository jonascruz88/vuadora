import Lutador from '../models/Lutador.js'
export default class uvcApi {
  constructor( url ) {
    this.url = url
  }
  //Método que retorna todos lutadores de uma categoria 
  async listarLutadoresPorCategoria( categoria ) {
    const lutadores = []
    return new Promise( resolve => {
      fetch( this.url )
        .then( j => j.json() )
        .then( p => {
          p.forEach(element => {
            if(element.weight_class === categoria){
              if(lutadores.length === 15){
              resolve( lutadores )
              }else{
              const lutador = new Lutador(element);
              lutadores.push(lutador)
              }
            }
          });
          resolve( lutadores )
        } )
    } )
  }
}
