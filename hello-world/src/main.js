import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Login from "./components/screens/Login.vue"
import VeeValidate, {Validator} from 'vee-validate'
import ptBR from 'vee-validate/dist/locale/pt_BR'
import CriacaoPersonagem from './components/screens/CriacaoPersonagem'
import MenuPrincipal from './components/screens/MenuPrincipal.vue'
import Treinamento from './components/screens/Treinamento.vue'
import PreLuta from './components/screens/PreLuta.vue'
import ResultadoLuta from './components/screens/ResultadoLuta.vue'
import Luta from './components/screens/Luta.vue'

Vue.config.productionTip = false

Validator.localize('pt_BR', ptBR)

Vue.use(VeeValidate)
Vue.use(VueRouter)

const routes = [ 
  { path: '/', component: Login },
  { name: 'CriacaoPersonagem', path: '/personagem', component: CriacaoPersonagem},
  { name: 'MenuPrincipal', path: '/menu', component: MenuPrincipal},
  { name: 'Treinamento', path: '/treinamento', component: Treinamento},
  { name: 'PreLuta', path: '/pre-luta', component: PreLuta},
  { name: 'ResultadoLuta', path: '/resultado', component: ResultadoLuta},
  { name: 'Luta' , path: '/luta' , component: Luta}
  
]

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
